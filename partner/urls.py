from rest_framework import routers
from .views import CategoryViewSet, PartnerViewSet, SocialNetworkViewSet

router = routers.SimpleRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'partners', PartnerViewSet )
router.register(r'social_networks', SocialNetworkViewSet )


urlpatterns = router.urls