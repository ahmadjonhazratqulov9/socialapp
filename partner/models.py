from django.db import models


class Partner(models.Model):
    full_name = models.CharField(max_length=255)
    photo = models.ImageField(upload_to='partner/')

    def __str__(self):
        return self.full_name


class Category(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class SocialNetwork(models.Model):
    YOU_TUBE = 'YOU_TUBE'
    TIKTOK = 'TIKTOK'
    TELEGRAM = 'TELEGRAM'
    INSTAGRAM = 'INSTAGRAM'

    SOCIAL_CHOISES = (
        (YOU_TUBE,YOU_TUBE),
        (TIKTOK,TIKTOK),
        (TELEGRAM,TELEGRAM),
        (INSTAGRAM,INSTAGRAM),
    )
    sub_count = models.IntegerField(default=0)
    url = models.URLField()
    name = models.CharField(choices=SOCIAL_CHOISES, max_length=255)
    price = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)


    def __str__(self):
        return f"{self.name} | {self.partner.full_name}"


