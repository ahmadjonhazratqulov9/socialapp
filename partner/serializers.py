from rest_framework import serializers
from .models import Partner, Category, SocialNetwork


class PartnerSerializers(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = '__all__'


class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class SocialNetworkSerializers(serializers.ModelSerializer):
    partner = PartnerSerializers
    category = CategorySerializers
    class Meta:
        model = SocialNetwork
        fields = '__all__'

""" hammasi zur"""

