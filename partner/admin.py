from django.contrib import admin
from .models import *

admin.site.register(Partner)
admin.site.register(Category)
admin.site.register(SocialNetwork)

